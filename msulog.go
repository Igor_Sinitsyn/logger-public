package logger

import (
	"context"
	"strconv"
	"time"

	"runtime/debug"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type contextKey uint

const (
	// TraceIDContextKey ключ для хранения trace-id в context
	TraceIDContextKey contextKey = iota

	// SessionIDContextKey ключ для хранения session-id в context
	SessionIDContextKey

	// UserContextKey ключ для хранения user в context
	UserContextKey

	// RequestURIContextKey ключ для хранения requestURI в context
	RequestURIContextKey

	// RemoteAddrContextKey ключ для хранения remoteAddr в context
	RemoteAddrContextKey
)

type ECSUser struct {
	Name string `json:"name"`
}

type ECSURL struct {
	Path  string `json:"path"`
	Query string `json:"query"`
}

type ECSRemote struct {
	IP string `json:"ip"`
}

type ECSError struct {
	Message    string `json:"message"`
	Stacktrace string `json:"stack_trace"`
}

type ECSEvent struct {
	Action    string    `json:"action"`
	Category  []string  `json:"category"`
	Code      string    `json:"code"`
	Created   time.Time `json:"created"`
	InnerType string    `json:"dataset"`
	Kind      string    `json:"kind"`
	Product   string    `json:"module"`
	Outcome   string    `json:"outcome"`
	Component string    `json:"provider"`
	Type      string    `json:"type"`
}

// AuditEvent аудит событие для пользовательских логов - когда событие зафиксировано в БД
type AuditEvent interface {
	Code() int
	Name() string
	String(lang int) string
}

type MsuLogger struct {
	logger    *zap.Logger
	Product   string
	Component string
}

func NewMsuLogger(logger *zap.Logger, product string, component string) *MsuLogger {
	return &MsuLogger{
		logger:    logger.WithOptions(zap.AddCallerSkip(1)),
		Product:   product,
		Component: component,
	}
}

func (logger *MsuLogger) Audit(ctx context.Context, event AuditEvent, message interface{}, info ...zapcore.Field) {

	loc, _ := time.LoadLocation("UTC")
	params := []zapcore.Field{
		zap.Any("event",
			ECSEvent{
				Action:    event.Name(),
				Created:   time.Now().In(loc),
				Category:  make([]string, 0),
				Code:      strconv.Itoa(int(event.Code())),
				InnerType: logger.Product + ".audit",
				Kind:      "",
				Product:   logger.Product,
				Outcome:   "success",
				Component: logger.Component,
				Type:      "",
			},
		),
		zap.Any(logger.Product, message),
	}

	params = append(params, info...)

	logger.lowLevelLog(logger.addRequestFields(ctx, zapcore.InfoLevel, params...))
}

func (logger *MsuLogger) Warn(ctx context.Context, err error, info ...zapcore.Field) {
	loc, _ := time.LoadLocation("UTC")
	params := []zapcore.Field{
		zap.Any("event",
			ECSEvent{
				Created:   time.Now().In(loc),
				Category:  make([]string, 0),
				InnerType: logger.Product + ".warn",
				Kind:      "",
				Product:   logger.Product,
				Outcome:   "unknown",
				Component: logger.Component,
				Type:      "",
			},
		),
		zap.Any("error",
			ECSError{
				Message:    err.Error(),
				Stacktrace: string(debug.Stack()),
			},
		),
	}

	params = append(params, info...)

	logger.lowLevelLog(logger.addRequestFields(ctx, zapcore.WarnLevel, params...))
}

func (logger *MsuLogger) Error(ctx context.Context, err error, info ...zapcore.Field) {
	loc, _ := time.LoadLocation("UTC")
	params := []zapcore.Field{
		zap.Any("event",
			ECSEvent{
				Created:   time.Now().In(loc),
				Category:  make([]string, 0),
				InnerType: logger.Product + ".error",
				Kind:      "",
				Product:   logger.Product,
				Outcome:   "failure",
				Component: logger.Component,
				Type:      "",
			},
		),
		zap.Any("error",
			ECSError{
				Message:    err.Error(),
				Stacktrace: string(debug.Stack()),
			},
		),
	}

	params = append(params, info...)

	logger.lowLevelLog(logger.addRequestFields(ctx, zapcore.ErrorLevel, params...))
}

func (logger *MsuLogger) Info(ctx context.Context, info ...zapcore.Field) {
	loc, _ := time.LoadLocation("UTC")
	params := []zapcore.Field{
		zap.Any("event",
			ECSEvent{
				Created:   time.Now().In(loc),
				Category:  make([]string, 0),
				InnerType: logger.Product + ".info",
				Kind:      "",
				Product:   logger.Product,
				Outcome:   "success",
				Component: logger.Component,
				Type:      "",
			},
		),
	}

	params = append(params, info...)

	logger.lowLevelLog(logger.addRequestFields(ctx, zapcore.InfoLevel, params...))
}

func (logger *MsuLogger) Fatal(ctx context.Context, err error, info ...zapcore.Field) {
	loc, _ := time.LoadLocation("UTC")
	params := []zapcore.Field{
		zap.Any("event",
			ECSEvent{
				Created:   time.Now().In(loc),
				Category:  make([]string, 0),
				InnerType: logger.Product + ".fatal",
				Kind:      "",
				Product:   logger.Product,
				Outcome:   "failure",
				Component: logger.Component,
				Type:      "",
			},
		),
		zap.Any("error",
			ECSError{
				Message:    err.Error(),
				Stacktrace: string(debug.Stack()),
			},
		),
	}

	params = append(params, info...)

	logger.lowLevelLog(logger.addRequestFields(ctx, zapcore.FatalLevel, params...))
}

func (logger *MsuLogger) addRequestFields(ctx context.Context, level zapcore.Level, info ...zapcore.Field) (zapcore.Level, []zapcore.Field) {
	params := info

	requestURI := ""
	if ctx.Value(RequestURIContextKey) != nil {
		requestURI = ctx.Value(RequestURIContextKey).(string)
	}
	remoteAddr := ""
	if ctx.Value(RemoteAddrContextKey) != nil {
		requestURI = ctx.Value(RequestURIContextKey).(string)
	}
	username := ""
	if ctx.Value(UserContextKey) != nil {
		username = ctx.Value(UserContextKey).(User).Name
	}

	params = append(params,
		[]zapcore.Field{zap.Any("url", ECSURL{Path: requestURI}),
			zap.Any("user", ECSUser{Name: username}),
			zap.Any("x-request-id", ctx.Value(TraceIDContextKey)),
			zap.Any("x-correlation-id", ctx.Value(SessionIDContextKey)),
			zap.Any("remote", ECSRemote{IP: remoteAddr})}...,
	)

	return level, params
}

func (logger *MsuLogger) lowLevelLog(level zapcore.Level, fields []zapcore.Field) {
	if level == zapcore.InfoLevel {
		logger.logger.Info("", fields...)
	} else if level == zapcore.ErrorLevel {
		logger.logger.Error("", fields...)
	} else if level == zapcore.WarnLevel {
		logger.logger.Warn("", fields...)
	} else if level == zapcore.FatalLevel {
		logger.logger.Fatal("", fields...)
	} else if level == zapcore.DebugLevel {
		logger.logger.Debug("", fields...)
	}
}
