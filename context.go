package logger

import "context"

func GetContextValue(ctx context.Context, key contextKey) string {
	if ctx.Value(key) != nil {
		return ctx.Value(key).(string)
	}

	return ""
}
