module gitlab.com/Igor_Sinitsyn/logger-public

go 1.16

require (
	github.com/jackc/pgx/v4 v4.10.1
	go.uber.org/zap v1.16.0
)
